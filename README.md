# README #

Dieses Projekt beinhaltet den schriftlichen Teil der Diplomarbeit von Thomas Fischbach. Die Arbeit wurde innerhalb der AG Larissa am Institut für Physik der Johannes Gutenberg Universität Mainz angefertigt.

Titel: **"Aufbau und Charakterisierung einer interferometrischen Frequenzstabilisierung für Diodenlaser"**

Universität: [Uni Mainz](https://uni-mainz.de/)

Institut: [Institut für Physik](http://www.iph.uni-mainz.de/)

Arbeitsgruppe: [AG LARISSA](https://www.larissa.physik.uni-mainz.de/)